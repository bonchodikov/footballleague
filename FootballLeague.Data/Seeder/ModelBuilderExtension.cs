﻿using FootballLeague.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FootballLeague.Data.Seeder
{
    public static class ModelBuilderExtension
    {

        public static void RelationshipSetter(this ModelBuilder model)
        {
            foreach (var relationship in model.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }

        public static List<Guid> GetId()
        {
            var res = new List<Guid>(40);
            for (int i = 0; i < res.Count; i++)
            {
                res.Add(Guid.NewGuid());
            }
            return res;
        }
        public static void Seeder(this ModelBuilder builder)
        {
            var res = GetId();

            //TO DO NOT USE for now I will FIX the SEEDER Someday...

            //builder.Entity<Team>().HasData
            //  (new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Hamburger SV",
            //      TotalScore = 1000,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Boca Juniors",
            //      TotalScore = 1001,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Manchester United",
            //      TotalScore = 1002,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Sao Paolo",
            //      TotalScore = 1003,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Arsenal",
            //      TotalScore = 1004,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Kispest Honved AC",
            //      TotalScore = 1005,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Chelsea ",
            //      TotalScore = 1006,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "PSV Eindhoven",
            //      TotalScore = 1007,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Dynamo Kiev",
            //      TotalScore = 1008,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Bayern Munich",
            //      TotalScore = 1009,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Real Madrid",
            //      TotalScore = 1010,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Borussia Dortmund",
            //      TotalScore = 1011,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Everton",
            //      TotalScore = 1012,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Penarol",
            //      TotalScore = 1013,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Barcelona",
            //      TotalScore = 1014,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Preston North End",
            //      TotalScore = 1015,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "River Plate",
            //      TotalScore = 1016,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Tottenham Hotspur",
            //      TotalScore = 1017,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Red Star Belgrade",
            //      TotalScore = 1042,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "AC Milan",
            //      TotalScore = 1018,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Ajax",
            //      TotalScore = 1019,
            //      IsDeleted = false,
            //  }, new Team
            //  {
            //      Id = new Guid(),
            //      Name = "Flamengo",
            //      TotalScore = 1020,
            //      IsDeleted = false,
            //  }) ;

            //builder.Entity<Rank>().HasData
            //   (new Rank
            //   {
            //       Id = res[0],
            //       TotalPoints = 10,
            //       IsDeleted = true,

            //   });
            //builder.Entity<Rank>(b =>
            //{
            //    b.HasKey(k => k.Id);                
            //    b.HasData(new Team { Id = Guid.NewGuid(), Name = "Hamburger SV", TotalScore = 1000, IsDeleted = false });                               
            //});
            var r = Guid.NewGuid();
            var e = Guid.NewGuid();
            builder.Entity<Team>().HasData
              (new Team
              {
                  Id = r,
                  Name = "Hamburger SV",
                  TotalScore = 1000,
                  IsDeleted = false,
                  RankId = res[1],
                  Rank = new Rank()
                  {
                      Id = e,
                      TotalPoints = 1,                      
                  }
              }) ;

        }
    }
}

﻿// <auto-generated />
using System;
using FootballLeague.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace FootballLeague.Data.Migrations
{
    [DbContext(typeof(FootballLeagueContext))]
    [Migration("20220904134331_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.17")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FootballLeague.Data.Entities.Match", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("EventTime")
                        .HasColumnType("datetime2");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<int>("Points")
                        .HasColumnType("int");

                    b.Property<Guid?>("RankId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Result")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("Winner")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.HasIndex("RankId");

                    b.ToTable("Matches");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Rank", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<int>("TotalPoints")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("Ranks");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Team", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("IsDeleted")
                        .HasColumnType("bit");

                    b.Property<Guid?>("MatchId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<Guid>("RankId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("TotalScore")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("MatchId");

                    b.HasIndex("RankId")
                        .IsUnique();

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Match", b =>
                {
                    b.HasOne("FootballLeague.Data.Entities.Rank", null)
                        .WithMany("PlayedMatches")
                        .HasForeignKey("RankId");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Team", b =>
                {
                    b.HasOne("FootballLeague.Data.Entities.Match", null)
                        .WithMany("TeamsPlaying")
                        .HasForeignKey("MatchId");

                    b.HasOne("FootballLeague.Data.Entities.Rank", "Rank")
                        .WithOne("Team")
                        .HasForeignKey("FootballLeague.Data.Entities.Team", "RankId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Rank");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Match", b =>
                {
                    b.Navigation("TeamsPlaying");
                });

            modelBuilder.Entity("FootballLeague.Data.Entities.Rank", b =>
                {
                    b.Navigation("PlayedMatches");

                    b.Navigation("Team");
                });
#pragma warning restore 612, 618
        }
    }
}

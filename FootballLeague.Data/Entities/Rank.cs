﻿using FootballLeague.Data.Entities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.Entities
{
    public class Rank :IRank
    {
        public Rank()
        {
            this.PlayedMatches = new List<Match>();
        }
        
        public Guid Id { get; set; }
        public int TotalPoints { get; set; }
        public Team Team { get; set; }

        public ICollection<Match> PlayedMatches { get; set; }
        public bool IsDeleted { get; set; }
    }
}

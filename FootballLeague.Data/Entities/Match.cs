﻿using FootballLeague.Data.Entities.IEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.Entities
{
    public class Match : IMatch
    {
        public Match()
        {
            this.TeamsPlaying = new List<Team>();
        }
        public Guid Id { get; set; }
        public Guid Winner { get; set; }
        public string Result { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime EventTime { get; set; }
        public int Points { get; set; }
        public ICollection<Team> TeamsPlaying { get; set; }
    }
}

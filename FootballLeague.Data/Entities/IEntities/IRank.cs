﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.Entities.IEntities
{
    public interface IRank
    {
        Guid Id { get; set; }
        int TotalPoints { get; set; }
        Team Team { get; set; }
        public bool IsDeleted { get; set; }
        ICollection<Match> PlayedMatches { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.Entities.IEntities
{
   public interface IMatch
   {
         Guid Id { get; set; }
         Guid Winner { get; set; }
         string Result { get; set; }
        public bool IsDeleted { get; set; }
        DateTime EventTime { get; set; }
         int Points { get; set; }
         ICollection<Team> TeamsPlaying { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Data.Entities.IEntities
{
    public interface ITeam
    {
         Guid Id { get; set; }
         string Name { get; set; }
         int TotalScore { get; set; }
         Rank Rank { get; set; }
        public bool IsDeleted { get; set; }
    }
}

﻿using FootballLeague.Data.Entities.IEntities;
using System;
using System.Collections.Generic;

namespace FootballLeague.Data.Entities
{
    public class Team : ITeam
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int TotalScore { get; set; }
        public Guid RankId { get; set; }
        public Rank Rank { get; set; }
        public bool IsDeleted { get; set; }
    }
}

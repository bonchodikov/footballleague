﻿using FootballLeague.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FootballLeague.Data.ConfigurationModels
{
    public class MatchConfig : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Points);
            builder.Property(p => p.Result);
            builder.HasMany(p => p.TeamsPlaying);           
        }
    }
}

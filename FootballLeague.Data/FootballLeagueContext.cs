﻿using FootballLeague.Data.ConfigurationModels;
using FootballLeague.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace FootballLeague.Data
{
    public class FootballLeagueContext : DbContext
    {
        public FootballLeagueContext(DbContextOptions<FootballLeagueContext> options) :
            base(options)
        { }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Rank> Ranks { get; set; }

        protected override void OnModelCreating(ModelBuilder model)
        {
            model.ApplyConfiguration(new RankConfig());
            model.ApplyConfiguration(new MatchConfig());
            model.ApplyConfiguration(new TeamConfig());
            //model.Seeder();
            base.OnModelCreating(model);
        }
    }
}

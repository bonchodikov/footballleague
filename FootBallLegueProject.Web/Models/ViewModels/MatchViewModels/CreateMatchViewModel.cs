﻿using FootballLeague.Services.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;
using PariplayFootballLeague.Web.Models.ViewModels;
using System;

namespace FootBallLegueProject.Web.Models.ViewModels.MatchViewModels
{
    public class CreateMatchViewModel
    {
        public TeamViewModel TeamOne { get; set; }
        public DateTime EventTime { get; set; }

        public TeamViewModel TeamTwo { get; set; }

      
    }
}

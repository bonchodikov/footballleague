﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace PariplayFootballLeague.Web.Models.ViewModels
{
    [BindProperties]
    public class TeamViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public int TotalScore { get; set; }
    }
}

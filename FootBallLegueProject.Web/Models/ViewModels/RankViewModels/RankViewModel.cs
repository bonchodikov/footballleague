﻿using PariplayFootballLeague.Web.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootBallLegueProject.Web.Models.ViewModels.RankViewModels
{
    public class RankViewModel
    {
        public int TotalPoints { get; set; }
        public TeamViewModel Team  { get; set; }
    }
}

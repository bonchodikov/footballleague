setupConnection = () => {
     connection = new signalR.HubConnectionBuilder()
        .withUrl("/UpdateHub")
        .build();
    connection.on("ReceiveListUpdate", (isChanged) => {
        $.ajax({
            async: true,
            type: "GET",
            url: '/Home/Index',
            success:

                function (partialView) {
                    $('#update').html(partialView);
                }
        });
    });

    connection.on("Finished", function () {
        connection.stop();
    });
    connection.start().catch(err => console.error(error.toString()));
}
setupConnection();
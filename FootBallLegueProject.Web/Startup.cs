using AutoMapperBuilder.Extensions.DependencyInjection;
using FootballLeague.Data;
using FootballLeague.Data.Entities;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.Services;
using FootBallLegueProject.Web.Factory;
using FootBallLegueProject.Web.Hubs;
using FootBallLegueProject.Web.Hubs.HubContracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NToastNotify;
using PariplayFootballLeague.Web.Mapper;
using System.Linq;

namespace FootBallLegueProject.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<FootballLeagueContext>(options => options.
          UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
           
            services.AddHttpContextAccessor();
            services.AddControllersWithViews().AddNToastNotifyToastr(new ToastrOptions()
            {
                ProgressBar = false,               
                PositionClass = ToastPositions.TopFullWidth
            });
            services.AddRazorPages();
            services.AddResponseCompression(ops => {
                ops.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(new[] { "application/octed-stream" });
            });
            services.AddScoped<IRankService, RankService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IMatchService, MatchService>();
            services.AddAbstractFactory<IMatch, Match>();
            services.AddAbstractFactory<IRank, Rank>();
            services.AddScoped<UpdateRankListHub>();
            services.AddMvc();
            services.AddSignalR();

            services.AddAutoMapper(typeof(Startup));
            //services.AddAutoMapperBuilder(builder =>
            //{
            //    builder.Profiles.Add(new AutoMaperConfig(services.BuildServiceProvider().GetRequiredService<ITeamService>()));
            //});

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseNToastNotify();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

          app.UseSignalR(routes => routes.MapHub<UpdateRankListHub>("/UpdateHub"));
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapHub<UpdateRankListHub>("/UpdateHub");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

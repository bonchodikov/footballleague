﻿using AutoMapper;
using FootballLeague.Data.Entities;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Web.Models.ViewModels.MatchViewModels;
using FootBallLegueProject.Web.Models.ViewModels.RankViewModels;
using PariplayFootballLeague.Web.Models.ViewModels;

namespace PariplayFootballLeague.Web.Mapper
{
    public class AutoMaperConfig : Profile
    {       
        public AutoMaperConfig()
        {
            
            this.CreateMap<TeamViewModel, BaseTeamDto>().ReverseMap();

            this.CreateMap<Team, BaseTeamDto>().ForMember(dest => dest.TeamName,
               opts => opts.MapFrom(src => src.Name)).ForMember(dest => dest.Id,
               opts => opts.MapFrom(src => src.Id)).ReverseMap();


            this.CreateMap<BaseTeamDto, TeamViewModel>().ForMember(dest => dest.Name,
               opts => opts.MapFrom(src => src.TeamName)).ForMember(dest => dest.Id,
               opts => opts.MapFrom(src => src.Id)).ForMember(dest => dest.TotalScore,
               opts => opts.MapFrom(src => src.Score)).ReverseMap();

            this.CreateMap<CreateMatchViewModel, CreateMatchDto>();

            this.CreateMap<Rank, RankDto>().ForMember(dest => dest.Team, opt => opt.MapFrom(src => new BaseTeamDto()
            {
                TeamName = src.Team.Name,
                Id = src.Id,
                Score = src.Team.TotalScore
            }));

            this.CreateMap<RankDto, RankViewModel>().AfterMap((src, dest) =>
            {
                dest.Team.Name = src.Team.TeamName;
                dest.Team.TotalScore = src.Team.Score;
            });
        }      
            
    }
}

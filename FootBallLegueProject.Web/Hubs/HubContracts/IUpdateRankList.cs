﻿using System.Threading.Tasks;

namespace FootBallLegueProject.Web.Hubs.HubContracts
{
    public interface IUpdateRankList
    {
        Task  Update(string name, bool isChanged);
    }
}

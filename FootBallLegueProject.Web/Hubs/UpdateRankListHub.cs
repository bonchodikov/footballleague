﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace FootBallLegueProject.Web.Hubs
{
    public class UpdateRankListHub : Hub
    {
        public async Task Update(IHubClients hubClients,bool isChanged)
        {
            if(isChanged)
            {
                await hubClients.All.SendAsync("ReceiveListUpdate", isChanged);
            }
            
        }
        public override Task OnConnectedAsync()
        {
            var connectionId = Context.ConnectionId;
            
            return base.OnConnectedAsync(); 
        }
    }
}

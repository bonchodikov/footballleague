﻿using AutoMapper;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Web.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PariplayFootballLeague.Web.Models.ViewModels;
using System.Threading.Tasks;

namespace FootBallLegueProject.Web.Controllers
{
    public class TeamController : Controller
    {
        private readonly ITeamService teamService;
        private readonly IMapper mapper;
        private readonly IHubContext<UpdateRankListHub> hubContext;
        private readonly UpdateRankListHub context;

        public TeamController(ITeamService teamService, IMapper mapper, IHubContext<UpdateRankListHub> hubContext, UpdateRankListHub context)
        {
            this.hubContext = hubContext;
            this.teamService = teamService;
            this.mapper = mapper;
            this.context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Create(TeamViewModel team)
        {
            if(teamService.Create(mapper.Map<BaseTeamDto>(team), mapper)!=null)
            {
                var p = context.Clients;
                var d = this.hubContext.Clients;
                 await context.Update(d, true);
            }
           
            return RedirectToAction("Index");

        }
    }
}

﻿using AutoMapper;
using FootballLeague.Services.Contracts;
using FootBallLegueProject.Web.Models;
using FootBallLegueProject.Web.Models.ViewModels.RankViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FootBallLegueProject.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRankService rankService;
        private readonly IMapper mapper;
        public HomeController(ILogger<HomeController> logger, IRankService rankService, IMapper mapper)
        {
            this.mapper = mapper; 
            this.rankService = rankService;
            _logger = logger;
        }

        public IActionResult Index()
        {
            var r = mapper.Map<IEnumerable<RankViewModel>>
                (this.rankService.GetAll(this.mapper)).AsEnumerable();
            return View("List", mapper.Map<IEnumerable<RankViewModel>>
                (this.rankService.GetAll(this.mapper)).AsEnumerable());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

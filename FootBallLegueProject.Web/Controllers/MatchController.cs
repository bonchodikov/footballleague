﻿using AutoMapper;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Web.Hubs;
using FootBallLegueProject.Web.Models.ViewModels.MatchViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using PariplayFootballLeague.Web.Models.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootBallLegueProject.Web.Controllers
{
    public class MatchController : Controller
    {
        private readonly IMapper mapper;
        private readonly IMatchService matchService;
        private readonly ITeamService teamService;
        private readonly UpdateRankListHub hubContext;
        private readonly IHubContext<UpdateRankListHub> hubContext1;

        public MatchController(IMapper mapper, 
            IMatchService matchService,
            ITeamService teamService, 
            UpdateRankListHub hubContext, 
            IHubContext<UpdateRankListHub> hubcontext1)
        {
            this.hubContext1
                 = hubcontext1;
            this.mapper = mapper;
            this.teamService = teamService;
            this.matchService = matchService;
            this.hubContext = hubContext;

        }
        public IActionResult Index()        
        
        {
            var teamViewModels = mapper.Map<ICollection<TeamViewModel>>(teamService.GetAll(mapper)).ToList();
            ViewData["Create"] = new SelectList(teamViewModels, "Id", "Name", "TotalScore");            
            return View("Create");
        }

        public async Task<IActionResult> Create(CreateMatchViewModel team)
        {
            if(await matchService.Create(mapper.Map<CreateMatchDto>(team), mapper))
            {
                var d = this.hubContext1.Clients;
                await hubContext.Update(d, true);                
            }         
            return RedirectToAction("Index");
        }

    }
}

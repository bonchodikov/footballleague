﻿using AutoMapper;
using FootballLeague.Data;
using FootballLeague.Data.Entities;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Services.Factory.IFactoryContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FootballLeague.Services.Services
{
    public class MatchService : IMatchService
    {
        private FootballLeagueContext database;
        private readonly IRankService rankService;
        private readonly ITeamService teamService;


        private readonly IAbstractFactory<IMatch> factory;
        public MatchService(FootballLeagueContext data,
            IAbstractFactory<IMatch> factory,
            IRankService rankService, ITeamService teamService)
        {
            this.database = data;
            this.factory = factory;
            this.rankService = rankService;
            this.teamService = teamService;
        }

        public async Task<bool> Guard(CreateMatchDto model)
        {

            if (model.TeamOne.Id == model.TeamTwo.Id || await database.Matches.AnyAsync(x => x.EventTime.Equals(model.EventTime) && x.TeamsPlaying
           .Any(q => q.Name.Equals(model.TeamOne.TeamName) && x.TeamsPlaying.Any(s => s.Name.Equals(model.TeamTwo.TeamName)))))
            {
                return false;
            }
            return true;

        }
        public async Task<bool> Create(CreateMatchDto model, IMapper mapper)
        {
            if (await this.Guard(model) is false)
            {
                return false;
            }
            var entityToCreate = this.factory.Create();
            
            entityToCreate = this.SetWinnerPoints(entityToCreate, model);
            entityToCreate.EventTime = model.EventTime;

            try
            {
                this.teamService.UpdateTeamsScore(model.TeamOne);
                this.teamService.UpdateTeamsScore(model.TeamTwo);
                entityToCreate.TeamsPlaying.Add(database.Teams.Include(b => b.Rank).FirstOrDefault(p => p.Id.Equals(model.TeamOne.Id)));
                entityToCreate.TeamsPlaying.Add(database.Teams.Include(b => b.Rank).FirstOrDefault(p => p.Id.Equals(model.TeamTwo.Id)));
                entityToCreate = this.SetResult(entityToCreate);
                this.rankService.UpdateRanks(entityToCreate);
                await this.database.Matches.AddAsync(entityToCreate as Match);
               await  database.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }

        private IMatch SetResult(IMatch model)
        {
            model.Result = String.Join("-", model.TeamsPlaying.Select(x => x.TotalScore));
            return model;
        }

        private IMatch SetWinnerPoints(IMatch matchToCreate,CreateMatchDto model)
        {
            if (model.TeamOne.Score > model.TeamTwo.Score || model.TeamOne.Score < model.TeamTwo.Score)
            {              
                matchToCreate.Points = 3;
                this.SetWinner(matchToCreate, model);
            }
            else
            {
                matchToCreate.Points = 1;
            }
            return matchToCreate;
        }

        private IMatch SetWinner(IMatch match, CreateMatchDto model)
        {
            if (model.TeamOne.Score > model.TeamTwo.Score)
            {
                match.Winner = model.TeamOne.Id;
                return match;
            }
            match.Winner = model.TeamTwo.Id;
            return match;
        }
        public bool DeleteById(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<CreateMatchDto> GetAll(IMapper mapper)
        {
            throw new NotImplementedException();
        }

        public CreateMatchDto GetById(Guid id, IMapper mapper)
        {
            throw new NotImplementedException();
        }

        public CreateMatchDto Update(CreateMatchDto model, IMapper mapper)
        {
            throw new NotImplementedException();
        }
    }
}

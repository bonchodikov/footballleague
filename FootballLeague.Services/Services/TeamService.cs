﻿using AutoMapper;
using FootballLeague.Data;
using FootballLeague.Data.Entities;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Services.Factory.IFactoryContracts;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using System;
using System.Collections.Generic;
using System.Linq;
namespace FootballLeague.Services.Services
{
    public class TeamService : ITeamService
    {
        private FootballLeagueContext database;
        private readonly IRankService rankServices;
        private readonly IToastNotification toastNotification;

        public TeamService(FootballLeagueContext data, IAbstractFactory<IRank> factory, IRankService rankService, IToastNotification toastNotification)
        {
            database = data;
            this.rankServices = rankService;
            this.toastNotification = toastNotification;
        }
        public BaseTeamDto Create(BaseTeamDto model, IMapper mapper)
        {
            if (database.Teams.Any(p => p.Name.Equals(model.TeamName)))
            {
                this.toastNotification.AddErrorToastMessage("Invalid, please chose a different name for the team");
                return null;
            }
            var res = new Team();
            res.Name = model.TeamName;            
            database.Teams.Add(res);
            res.Rank = this.rankServices.Create(res);            
            var result = mapper.Map<BaseTeamDto>(res);
            database.SaveChanges();
            this.toastNotification.AddSuccessToastMessage("The Team is Created successfuly!");
            return result;
        }

        public bool DeleteById(int id)
        {
            throw new System.NotImplementedException();
        }

        public ICollection<BaseTeamDto> GetAll(IMapper mapper)
        {
            var rowData = database.Teams.Include(p => p.Rank);
            if (rowData != null)
            {
                var res = mapper.Map<ICollection<BaseTeamDto>>(rowData);

                return res;
            }
            return null;
        }

        

        public BaseTeamDto GetById(Guid id, IMapper mapper)
        {
            var res = database.Teams.FirstOrDefault(p => p.Id.Equals(id));
            if (res != null)
            {
                var result = mapper.Map<BaseTeamDto>(res);

                //TODO
                //result.TeamTotalGolsCount = res.PlayersInTeam.Count;
                return result;
            }
            return null;
        }

        public string GetById(Guid id)
        {
            return database.Teams.FirstOrDefault(p => p.Id.Equals(id)).Name;
        }
             

        public void UpdateTeamsScore(BaseTeamDto model)
        {
            this.database.Teams.FirstOrDefault(x => x.Id.Equals(model.Id)).TotalScore += model.Score;
        }
    }
}

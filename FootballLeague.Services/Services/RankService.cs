﻿using AutoMapper;
using FootballLeague.Data;
using FootballLeague.Data.Entities;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.Contracts;
using FootballLeague.Services.DTOs;
using FootBallLegueProject.Services.Factory.IFactoryContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FootballLeague.Services.Services
{
    public class RankService : IRankService
    {
        private FootballLeagueContext database;
        private readonly IAbstractFactory<IRank> abstractFactory;
        public RankService(FootballLeagueContext database, IAbstractFactory<IRank> abstractFactory)
        {
            this.abstractFactory = abstractFactory;
            this.database = database;
        }      

        public void UpdateRanks(IMatch match)
        {
            if (match.Winner == null || match.Winner == Guid.Empty)
            {
                foreach (var team in match.TeamsPlaying)
                {
                   this.database.Ranks.Where(x => x.Team.Id.Equals(team.Id))
                        .FirstOrDefault()
                        .TotalPoints+=match.Points;                    
                }
                this.database.SaveChanges();
                return;
            }
            this.database.Ranks.Where(x => x.Team.Id.Equals(match.Winner))
                         .FirstOrDefault()
                         .TotalPoints += match.Points;
            
            this.database.SaveChanges();
            return;
        }

        public Rank Create(Team team)
        {
            var rankToCreate = this.abstractFactory.Create();
            rankToCreate.Team = team;
            this.database.Ranks.Add(rankToCreate as Rank);
            this.database.SaveChanges();
            return database.Ranks.FirstOrDefault(x => x.Team.Id.Equals(team.Id));
        }

        public IEnumerable<RankDto> GetAll(IMapper mapper)
        {
            var x = database.Ranks.Where(x => x.IsDeleted != true)
                .Include(x => x.Team).OrderByDescending(x => x.TotalPoints)
                .AsEnumerable();

            return mapper.Map<IEnumerable<RankDto>>
                (database.Ranks.Where(x=>x.IsDeleted!=true)
                .Include(x=>x.Team).OrderByDescending(x=>x.TotalPoints).ThenByDescending(x=>x.Team.TotalScore)
                .AsEnumerable());
        }        
    }
}

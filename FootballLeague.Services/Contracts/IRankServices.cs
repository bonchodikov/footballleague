﻿using AutoMapper;
using FootballLeague.Data.Entities;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.DTOs;
using System.Collections.Generic;

namespace FootballLeague.Services.Contracts
{
    public interface IRankService
    {
        void UpdateRanks(IMatch team);
         Rank Create(Team team);
        IEnumerable<RankDto> GetAll(IMapper mapper);
    }
}

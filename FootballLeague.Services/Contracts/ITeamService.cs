﻿using AutoMapper;
using FootballLeague.Data.Entities.IEntities;
using FootballLeague.Services.DTOs;
using System;
using System.Collections.Generic;

namespace FootballLeague.Services.Contracts
{
    public interface ITeamService
    {
        ICollection<BaseTeamDto> GetAll(IMapper mapper);
        BaseTeamDto GetById(Guid id, IMapper mapper);
        BaseTeamDto Create(BaseTeamDto model, IMapper mapper);

        void UpdateTeamsScore(BaseTeamDto model);
        bool DeleteById(int id);

        public string GetById(Guid id);
             

    }
}

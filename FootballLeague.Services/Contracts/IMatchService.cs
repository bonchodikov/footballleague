﻿using AutoMapper;
using FootballLeague.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FootballLeague.Services.Contracts
{
    public interface IMatchService
    {
        ICollection<CreateMatchDto> GetAll(IMapper mapper);
        CreateMatchDto GetById(Guid id, IMapper mapper);
        Task<bool> Create(CreateMatchDto model, IMapper mapper);
        CreateMatchDto Update(CreateMatchDto model, IMapper mapper);
        bool DeleteById(int id);
    }
}

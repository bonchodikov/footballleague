﻿namespace FootBallLegueProject.Services.Factory.IFactoryContracts
{
    public interface IAbstractFactory<T>
    {
        public T Create();
    }
}

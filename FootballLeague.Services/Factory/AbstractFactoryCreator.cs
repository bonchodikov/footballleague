﻿using FootBallLegueProject.Services.Factory.IFactoryContracts;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace FootBallLegueProject.Web.Factory
{
    public static class AbstractFactoryCreator
    {
        public static void AddAbstractFactory<TInterface, TClass>(this IServiceCollection serviceCollection)
            where TInterface : class
            where TClass : class, TInterface           

        {
            serviceCollection.AddTransient<TInterface, TClass>();
            serviceCollection.AddSingleton<Func<TInterface>>(x => () => x.GetService<TInterface>()!);
            serviceCollection.AddSingleton<IAbstractFactory<TInterface>, AbstractFactory<TInterface>>();

        }
    }
}

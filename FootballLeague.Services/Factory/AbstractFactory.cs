﻿using FootBallLegueProject.Services.Factory.IFactoryContracts;
using System;

namespace FootBallLegueProject.Web.Factory
{
    public class AbstractFactory<T> : IAbstractFactory<T>
    {
        private readonly Func<T> factory;
        public AbstractFactory(Func<T> factory)
        {
            this.factory = factory;
        }
        public T Create()
        {
            return this.factory();
        }
    }
}

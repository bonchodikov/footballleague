﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Services.DTOs
{
    public class BaseTeamDto
    {
        public Guid Id { get; set; }
        public int Score { get; set; }
        public string TeamName { get; set; }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Services.DTOs
{
    public class RankDto
    {
        public Guid Id { get; set; }
        public BaseTeamDto Team { get; set; }
        public int TotalPoints { get; set; }
    }
}

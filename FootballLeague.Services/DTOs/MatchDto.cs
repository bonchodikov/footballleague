﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Services.DTOs
{
    public class MatchDto
    {
        public int Points { get; set; }
        public ICollection<BaseTeamDto> TeamsPlaying { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballLeague.Services.DTOs
{
    public class CreateMatchDto
    {
        public BaseTeamDto TeamOne { get; set; }
        public DateTime EventTime { get; set; }

       public BaseTeamDto TeamTwo { get; set; }

    }
}
